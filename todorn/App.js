import React, { Component } from 'react'
import { View , Modal , Text , Alert } from 'react-native'
import Axios from 'axios';
import { Input, Container, Content, Card, CardItem, Button, Right} from 'native-base';


export default class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: [],
      judul: '',
      _id: '',
      state: '',
      id: '',
      modalVisible: false,
      modalVisibles: false
    }
    
  }

  componentDidMount = () => {
    this.datas()
    setInterval(this.datas, 1000)
    // console.log('props data',this.props.data)
  }

  

  datas = () => {
    Axios.get(`https://btm-rn.herokuapp.com/api/v1/todo`)
      .then(res => {
        this.setState({
          data: res.data.results
        })
      })
  }


  inputData = () => {
    const tambah = async objParam => await Axios.post(
      `https://btm-rn.herokuapp.com/api/v1/todo`, objParam
    )

    tambah({
      title: this.state.judul
    })
      .then(res => {
        console.log(res.data)
      })
      .catch(e => console.log(e))

    this.datas();
  }

  editData = async (id, objParam) => {
    await Axios.put(
      `https://btm-rn.herokuapp.com/api/v1/todo/${id}`, objParam
    )
  }

  deleteData = async id => await Axios.delete(
    `https://btm-rn.herokuapp.com/api/v1/todo/${id}`
  )

  testrue = (isCom) => {
    if (isCom === true) {
      return false
    } else if (isCom === false) {
      return true
    } else {
      console.log(`Jangan kesini Sayang :(`)
    }
  }

  setModalVisible = (visible) => {
    this.setState({ modalVisible: visible })
  }

  getModalVisible = (visible) => {
    this.setState({ modalVisibles: visible })
  }

  toggle(id , status , title){
    this.setState({
      id : id
    })
    this.setState({
      state : status
    })
    this.setState({
      judul: title
    })
  }



  render() {

    const list = this.state.data.map(hasil => {
      console.log(hasil)
      return (
        <>

          <Card key={hasil._id}>
            <CardItem>
              <Text>{hasil.title}</Text>
              <Right>
                <Button onPress={
                 () => this.deleteData(hasil._id)}><Text>Hapus</Text></Button>
               
               <Button onPress={
                 () => {this.toggle(hasil._id , hasil.isComplete , hasil.title)
                  this.getModalVisible(true)}}>
                <Text>Edit</Text>
                </Button>
              </Right>
            </CardItem>
          </Card>

        </>
      )
    });



    return (
      <Container>
        <Content>
        <Button
          onPress={() => {
            this.setModalVisible(true);
          }}>
          <Text>Show Modal</Text>
        </Button>

          {list} 

              <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}
          
          >
          <View style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center'}}>
            <View style={{
            width: 300,
            height: 300, backgroundColor: '#ccc'}}>
              <Text style={{color:'white'}}>Hello World!</Text>
              <Input valid placeholder="Input Disini yaa" onChangeText={(text) => this.setState({judul : text})}  />
              <Button
                onPress={() => {
                  this.setModalVisible(!this.state.modalVisible);
                  this.inputData()
                }}>
                <Text>Tambah Data</Text>
              </Button>
              <Button
                onPress={() => {
                  this.setModalVisible(!this.state.modalVisible);
                }}>
                <Text>Cancel</Text>
              </Button>
            </View>
          </View>
        </Modal> 

 {/* ACTION EDIT */}
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisibles}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}
          
          >
          <View style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center'}}>
            <View style={{
            width: 300,
            height: 300, backgroundColor: '#ccc'}}>
              <Text style={{color:'white'}}>Hello World!</Text>
              <Input valid  onChangeText={(text) => this.setState({judul : text})} value={this.state.judul}  />
              <Button
                onPress={() => {
                 
                  this.getModalVisible(!this.state.modalVisibles)
                  this.editData(
                    this.state.id,
                    {
                      title: this.state.judul,
                      isComplete: this.state.state
                    }
                  )
                }}>
                <Text>Update</Text>
              </Button>
              <Button
                onPress={() => {
                  this.getModalVisible(!this.state.modalVisibles);
                }}>
                <Text>Cancel</Text>
              </Button>
            </View>
          </View>
        </Modal> 
     </Content>
     </Container>

   
     

    )
  }
}

